all: k-means

clean:
	rm k-means

k-means: k-means.cpp
	clang++ -std=c++11 -O3 -o $@ $^
